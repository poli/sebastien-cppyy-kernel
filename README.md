# CppYY Kernel


## Installation

**NB:** requires python v3.x (currently tested only in a Linux/miniconda3 environment with python=3.12)

* Install jupyterlab
* Then:
  ```sh
  pip install git+https://gitlab.inria.fr/sed-saclay/cppyy_kernel.git@v0.1.0
  ```
  (replace `v0.1.0` by the latest version)

  Or, if you would like to perform a developer install:

  ```sh	
  git clone https://gitlab.inria.fr/sed-saclay/cppyy_kernel
  # or (with ssh)
  git clone git@gitlab.inria.fr:sed-saclay/cppyy_kernel.git
  # install the package from sources
  pip install ./cppyy_kernel
  # or (for a dev environment)
  pip install -e ./cppyy_kernel
  ```

## Usage

To use this kernel, install `jupyter lab` or `jupyter notebook` then run one of them:

```sh	
jupyter lab  # or
jupyter notebook
```

**NB:** This kernel is based on [MetaKernel](http://pypi.python.org/pypi/metakernel), which means it features a standard set of magics (such as %%html). For a full list of magics, run %lsmagic in a cell.

A sample notebook is available in `./notebooks/SimpleTest.ipynb`.

## Configuring cppyy for a particular C++ standard

Configuring cppyy for a particular C++ standard requires additional settings. See [C++ standard with pip](https://cppyy.readthedocs.io/en/latest/installation.html#c-standard-with-pip).


## Jupyter hints

* Listing installed kernels:
  ```sh
  jupyter kernelspec list
  ```

* Uninstalling a kernel:
  ```sh
  jupyter kernelspec uninstall unwanted_kernel
  ```

## See Also

- [xeus-cpp](https://xeus-cpp.readthedocs.io/en/latest/): a successor to xeus-cling, based on [clang-repl](https://clang.llvm.org/docs/ClangRepl.html)

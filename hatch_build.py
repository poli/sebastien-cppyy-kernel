import sys
from pathlib import Path
from hatchling.builders.hooks.plugin.interface import BuildHookInterface
from jupyter_client.kernelspec import KernelSpecManager


class CustomHook(BuildHookInterface):
    def initialize(self, version, build_data):
        here = Path(__file__).parent.resolve()
        sys.path.insert(0, here)
        data = str(here / "data")
        jupyter_data = str(here / "jupyter-data")
        KernelSpecManager().install_kernel_spec(data, 'cppyy',
                                                user=False,
                                                prefix=jupyter_data)

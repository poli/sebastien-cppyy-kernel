from metakernel import MetaKernel
import cppyy
import io
from contextlib import redirect_stderr, redirect_stdout
from wurlitzer import sys_pipes

from . import __version__

class CppyyKernel(MetaKernel):
    implementation = 'Cppyy'
    implementation_version = __version__
    language = 'c++'
    language_version = f"cppyy_{cppyy.__version__}"
    language_info = {
        "name": "c++",
        "codemirror_mode": "c++",
        "mimetype": "text/x-c++src",
        "file_extension": '.cpp'
    }
    banner = "Cppyy kernel"

    # Letting the initial '?' led to spurious behaviour when command was ended by a '?',
    # even if in a comment.
    help_suffix = "_We don't want any!_"

    def do_execute_direct(self, code):
        if not code.strip(" \r\n"):
            return
        lcode = code.split("\n")
        if lcode[-1].startswith("#include") and code[-1] != "\n":
            code += "\n"  # avoids a warning "extra tokens at end of #include directive"
        buff = io.StringIO()
        buff_err = io.StringIO()
        ret = None
        with redirect_stderr(buff_err), redirect_stdout(buff):  # actually only stderr is used
            try:
                # from https://notebook.community/minrk/wurlitzer/Demo
                with sys_pipes() as (stdout, _):
                    ret = cppyy.cppexec(code)
                if out := stdout.getvalue().strip("\r\n"):
                    self.Print(out, end="")
            except Exception as exc:
                self.Error(exc.args[0], end="\n")
        if (bout := buff.getvalue().strip("\r\n")) and bout != out:
            self.Print(bout, end="")
        if berr := buff_err.getvalue():
            self.Error(berr, end="")

        return None if ret is True else ret
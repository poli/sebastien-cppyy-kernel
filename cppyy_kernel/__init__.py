"""Cppyy Jupyter kernel"""


from ._version import __version__
from .kernel import CppyyKernel
